# Ansible Role Template for Services on Linux

This is a Ansible Role Template for Linux OS mainly Ubuntu. The Role is tested against a CI/CD Pipeline in Gitlab and have some Tags on the Tasks to Impelement some capabilities for a devops style deployment. 

Requirements:
    None

Role Variables:
    See in the defaults Directory

Example Playbook:
```YAML
- hosts: all
  gather_facts: yes
  roles:
     - ansible-role-template-linux
```

Tested:
 - Ubuntu 18.04
 - Ubuntu 20.04
License:
    MIT / BSD

Author Information:
roland@stumpner.at